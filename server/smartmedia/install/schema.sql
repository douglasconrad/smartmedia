CREATE TABLE `smartmedia_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `smartmedia_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `host` varchar(20) DEFAULT NULL,
  `groupId` int(5) NOT NULL,
  `hostkey` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clients_groupId` (`groupId`),
  CONSTRAINT `fk_clients_groupId` FOREIGN KEY (`groupId`) REFERENCES `smartmedia_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `smartmedia_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `groupId` int(5) NOT NULL,
  `fullPath` varchar(120) DEFAULT NULL,
  `description` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_files_groupId` (`groupId`),
  CONSTRAINT `fk_files_groupId` FOREIGN KEY (`groupId`) REFERENCES `smartmedia_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;