<?php

/**
 *  This file is part of SNEP.
 *
 *  SNEP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SNEP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SNEP.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SmartMedia Controller
 *
 * @category Snep
 * @package Snep
 * @copyright Copyright (c) 2013 OpenS Tecnologia
 * @author Kaue Santos
 */
class SmartMedia_GroupsController extends Zend_Controller_Action {
	
	
	/**
	 * Lista Grupos
	 */
	public function indexAction() {
		
		$this->view->breadcrumb = $this->view->translate ( "SmartMedia » Grupos" );
		if ($this->_request->getPost ( 'filtro' )) {
			$field = $this->_request->getPost ( 'campo' );
			$query = $this->_request->getPost ( 'filtro' );
		}
		
		if (! isset ( $query )) {
			$query = "";
			$field = "";
		}
		
		$groups = SmartMedia_Group_Manager::getAllFilter ( $query, $field );
		
		$page = $this->_request->getParam ( 'page' );
		$this->view->page = (isset ( $page ) && is_numeric ( $page ) ? $page : 1);
		$this->view->filtro = $this->_request->getParam ( 'filtro' );
		
		$paginatorAdapter = new Zend_Paginator_Adapter_Array ( $groups );
		$paginator = new Zend_Paginator ( $paginatorAdapter );
		$paginator->setCurrentPageNumber ( $this->view->page );
		$paginator->setItemCountPerPage ( Zend_Registry::get ( 'config' )->ambiente->linelimit );
		
		$this->view->pathweb = Zend_Registry::get ( 'config' )->system->path->web;
		$this->view->groups = $paginator;
		$this->view->pages = $paginator->getPages ();
		$this->view->url = $this->getFrontController ()->getBaseUrl () . "/" . $this->getRequest ()->getModuleName () . "/groups/";
		
		$this->view->PAGE_URL = $this->getFrontController ()->getBaseUrl () . "/" . $this->getRequest ()->getModuleName () . "/" . $this->getRequest ()->getControllerName () . "/index/";
		$options = array (
				"group" => $this->view->translate ( "Grupo" ) 
		);
		
		// Formulário de filtro.
		$filter = new Snep_Form_Filter ();
		$filter->setAction ( $this->getFrontController ()->getBaseUrl () . '/smartmedia/groups' );
		$filter->setValue ( $this->_request->getPost ( 'campo' ) );
		$filter->setFieldOptions ( $options );
		$filter->setFieldValue ( $this->_request->getParam ( 'filtro' ) );
		$filter->setResetUrl ( "{$this->getFrontController()->getBaseUrl()}/smartmedia/groups" );
		
		$this->view->form_filter = $filter;
		$this->view->filter = array (
				array (
						"url" => $this->view->url . "add",
						"display" => $this->view->translate ( "Incluir Grupo" ),
						"css" => "include" 
				) 
		);
	}
	
	/**
	 * Adiciona grupo
	 */
	public function addAction() {
		
		$form = new Snep_Form ( new Zend_Config_Xml ( "modules/smartmedia/forms/groups.xml" ) );
		
		$id = $this->_request->getParam ( 'id' );
		
		if ($this->_request->getPost () && $form->isValid ( $_POST )) {
			
			$id = SmartMedia_Group_Manager::add( $_POST );
			
			$path = APPLICATION_PATH . '/modules/' . $this->getRequest ()->getModuleName () . "/media/{$id}";
			
			exec ( "mkdir " . $path );
			exec ( "chmod -R 777 " . $path );
			
			$this->_redirect ( $this->getRequest ()->getModuleName () . '/' . $this->getRequest ()->getControllerName () );
		}
		
		$form->setButton ();
		$this->view->form = $form;
	}
	
	/**
	 * EditaGrupo
	 */
	public function editAction() {
		
		$form = new Snep_Form ( new Zend_Config_Xml ( "modules/smartmedia/forms/groups.xml" ) );
		
		$id = $this->_request->getParam ( 'id' );
		$dados = SmartMedia_Group_Manager::get( $id );
		
		if ($dados) {
			$form->setDefaults ( $dados );
		}
		
		if ($this->_request->getPost () && $form->isValid ( $_POST )) {
			SmartMedia_Group_Manager::edit( $_POST, $id );
			$this->_redirect ( $this->getRequest ()->getModuleName () . '/' . $this->getRequest ()->getControllerName () );
		}
		
		$form->setButton ();
		$this->view->form = $form;
	}
	
	/**
	 * Remove grupo
	 */
	public function removeAction() {
		$id = $this->_request->getParam ( 'id' );
		
		SmartMedia_Group_Manager::remove( $id );
		
		$path = APPLICATION_PATH . '/modules/' . $this->getRequest ()->getModuleName () . "/media/{$id}";
		exec ( "rm -rf " . $path );
		
		$this->_redirect ( $this->getRequest ()->getModuleName () . '/' . $this->getRequest ()->getControllerName () );
	}
}
