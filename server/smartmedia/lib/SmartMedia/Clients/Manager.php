<?php
/**
 * Client Manager for Clients
 * @author Douglas Conrad
 */
class SmartMedia_Clients_Manager {
	
	/**
	 * Adiciona um cliente
	 * 
	 * @param array $data
	 */
	public static function add($data) {
		$db = Zend_Registry::get ( 'db' );
		$insert_data = array (
				'name' => $data ['name'],
				'host' => $data ['host'],
				'groupId' => $data ['group'],
				'hostkey' => $data ['hostkey'] 
		);
		
		$db->insert ( 'smartmedia_clients', $insert_data );
	}
	
	/**
	 * retorna um cliente
	 * 
	 * @param int $id
	 * @return array da tupla do evento
	 */
	public static function get($id) {
		$db = Zend_Registry::get ( 'db' );
		
		$select = $db->select ()->from ( "smartmedia_clients" )->where ( 'id = ?', $id );
		
		return $db->query ( $select )->fetch ();
	}
	
	/**
	 * Retorna todos os clientes
	 * 
	 * @return array
	 */
	public static function getAll() {
		$db = Zend_registry::get ( 'db' );
		
		$select = $db->select ()->from ( "smartmedia_clients" );
		
		return $db->query ( $select )->fetchAll ();
	}
	/**
	 * getAllFilter - Rotina de filtro de busca do Snep
	 * 
	 * @param <String> $query - Valor do nome ou código
	 * @param <String> $field - Tipo de busca (name, host ou group)
	 * @return <Array> $clientsData - Array de clients conforme busca
	 *
	 */
	public static function getAllFilter($query, $field) {
		
		$db = Zend_registry::get ( 'db' );
		if ($query != null) {
			if ($field == "name") {
				$clientsData = $db->query ( "select sc.id as `id`, sc.name as `name`,sc.host as `host` ,sc.hostkey as `hostkey`,sg.name as `group` from smartmedia_clients sc join smartmedia_groups sg on sc.groupId = sg.id where name like '%$query%'" )->fetchAll ();
			} elseif ($field == "host") {
				$clientsData = $db->query ( "select sc.id as `id`, sc.name as `name`,sc.host as `host` ,sc.hostkey as `hostkey`,sg.name as `group` from smartmedia_clients sc join smartmedia_groups sg on sc.groupId = sg.id  where host like '%$query%'" )->fetchAll ();
			} elseif ($field == "group") {
				$clientsData = $db->query ( "select sc.id as `id`, sc.name as `name`,sc.host as `host` ,sc.hostkey as `hostkey`,sg.name as `group` from smartmedia_clients sc join smartmedia_groups sg on sc.groupId = sg.id  where sc.groupId='$query'" )->fetchAll ();
			}
		} else {
			$clientsData = $db->query ( "select sc.id as `id`, sc.name as `name`,sc.host as `host` ,sc.hostkey as `hostkey`,sg.name as `group` from smartmedia_clients sc join smartmedia_groups sg on sc.groupId = sg.id" )->fetchAll ();
		}
		return $clientsData;
	}
	
	/**
	 * edita um cliente
	 * 
	 * @param array $data
	 * @param int $id
	 */
	public static function edit($data, $id) {
		$db = Zend_Registry::get ( 'db' );
		$update_data = array (
				'id' => $id,
				'name' => $data ['name'],
				'host' => $data ['host'],
				'groupId' => $data ['group'],
				'hostkey' => $data ['hostkey'] 
		);
		
		$db->update ( "smartmedia_clients", $update_data, array (
				'id = ?' => $id 
		) );
	}
	
	public static function remove($id) {
		$db = Zend_Registry::get ( 'db' );
		return $db->delete ( "smartmedia_clients", array (
				"id = ?" => $id 
		) );
	}
	
}
	
