# variaveis de ambiente
basedir=`dirname $0`
debug=0
synctimeloop=5
pidfile=/tmp/smartmedia.pid
mpgfifo=/tmp/mpgctl
mpglogfile=/tmp/mpg.log
mpgerrorfile=/tmp/mpg.error
mpg123=/usr/bin/mpg123
rsync=/usr/bin/rsync
amixer=/usr/bin/amixer
musicdevice="3 1"
PID=$$
echo $PID > $pidfile
app=`basename $0`


check(){
	for arg in $@
	do
		if [ "$arg" == "-d" ]
		then
			debug=1
		fi
		if [ "$arg" == "stop" ]
		then
			stopapp=1
		fi

	done

	logfile=/tmp/smartmedia.log

	if [ ! -x $rsync ]
	then
		debug=1
		log "Programa [rsync] nao encontrado!"
		log "Saindo da Aplicação"
		exit 1
	elif [ ! -x $mpg123 ]
	then
		debug=1
		log "Programa [mpg123] nao encontrado!"
		log "Saindo da Aplicação"
		exit 1
	fi
	load
}
printhelp() {
	echo -en "ERRO: Voce deve informar a origem e o destino da sincronizacao.\n"
	echo -en "Use:
	$0 [OPCOES] ORIGEM DESTINO
	Opcoes:
		-d : habilita um debug de tela

	Exemplo de uso:
		$0 usuario@192.168.10.1:/home/usuario/Musicas /home/Musicas\n\n"
}

log(){
	if [ $debug -eq 1 ]
	then
		echo -en "`date` - SMARTMEDIA - $1\n"
	fi
	echo -en "`date` - SMARTMEDIA - $1\n" >> $logfile
	PID=$$
	echo $PID > $pidfile.log
}

readconfig(){
	if [ ! -f $basedir/$conffile ]
	then
		log "ERRO: arquivo de configuração [$basedir/$conffile] nao existe!"
		log "Saindo da Aplicação"
		exit 1
	fi
	. $basedir/$conffile
	log "Configuracoes padroes carregadas"
}

testconnection(){
	server=$1
	log "Testando a conexao com a ORIGEM: $server"
	status=`ping -q -c 1 $server|grep "packet loss"|awk '{print $6}'|sed s/%//g`
	if [ "$status" -gt "0" ]
	then
		debug=1
		log "ERRO de conexao com a ORIGEM: $server"
		#log "Saindo da Aplicação"
		#exit 1
	else
		log "Conexao com a ORIGEM: $server OK"
	fi
}

gethost(){
	echo $sourceroot|awk -F: '{print $1}'|awk -F@ '{print $2}'
}

load(){
	log "Lendo configuracoes padroes..."
	readconfig
	#log "Carregando as chaves de autenticação..."
	#eval `ssh-agent -s`
	#ssh-add $private_key
	log "Ajustando dispositivo de music para $music"
	$amixer cset numid=$musicdevice
}
sync(){
	PID=$$
	echo $PID > $pidfile.sync
	testconnection $defaultmediaserver
	log "Iniciando sincronia..."
	if [ "x$deletefromdest" == "xyes" ]
	then
		delete="--delete"
	fi
	log "Sincronizando musicas..."
	export RSYNC_PASSWORD=$defaultpassword
	$rsync -razql --no-motd -e "ssh -o 'StrictHostKeyChecking=no'" $defaultuser@$defaultmediaserver:$remotemusicpath $musicpath >> $logfile 2>> $logfile
	log "Sincronizando videos..."
	$rsync -razql --no-motd -e "ssh -o 'StrictHostKeyChecking=no'" $defaultuser@$defaultmediaserver:$remotevideopath $videopath >> $logfile 2>> $logfile

}

loadmusics(){
	log "Criando Playlist..."
	playlist=$musicpath/music-playlist
	find $musicpath -type f -iname \*.mp3 > $playlist
	tracknumbers=`cat $playlist|wc -l`
	track=0
	log "Playlist criada com $tracknumbers musicas"
}

readmpg(){
	pidmpg=`ps -C mpg123 -o pid=`
	echo >  $mpglogfile
	log "Checando mpg123 daemon..."
	if [ "x$pidmpg" != "x" ]
	then
		log "mpg123 ja rodando"
	else
		log "Iniciando daemon mpg123..."
		rm -f $mpgfifo
		$mpg123 -R --fifo $mpgfifo > $mpglogfile 2> $mpgerrorfile &
	fi
	log "Iniciando scanner de musicas..."
	exec 3<$mpglogfile 
	while read -u3 line 
	do 
		if [[ $(echo "$line" |grep -c "@P 0") -eq 1 ]]
		then 
			playmusic 
		fi 
	done	
		
}

playmusic(){
	if [ $track -ge $tracknumbers ]
	then
		log "A Playlist chegou ao final das musicas. Total de $tracknumbers musicas foram tocadas"
		log "Reiniciando a Playlist..."
		track=0
	fi
	if [ "$musicaction" == "start" ]
	then
		(( track++ ))
		log "Iniciando a musica $track da playlist $playlist...usando fifo $mpgfifo"
		echo "LOADLIST $track $playlist" > $mpgfifo
		log "Feito"
	fi
}

runmusic(){
	savedstarttime=`echo $musictimestart|sed s,:,,g`
	savedendtime=`echo $musictimeend|sed s,:,,g`
	now=`date '+%H%M'`
	musicaction="ignore"
	if [ $musicloop -ge 1 ]
	then
		if [ "$now" -ge "$savedendtime" ]
		then
				musicaction="stop"
		else
				musicaction="ignore"
		fi
	elif [ $now -ge $savedstarttime ] && [ $now -le $savedendtime ]
	then
		log "Ajustando para iniciar musicas...musicloop=${musicloop} , start time=${savedstarttime} , end time=${savedendtime}, now=${now}"
		musicaction="start"
		musicloop=$(expr $musicloop + 1)
	else
		if [ "x$pidmpg" != "x" ]
		then 
			musicaction="stop"
		else
			musicaction="ignore"
		fi
	fi

	case $musicaction in
		start)
			log "Iniciando musicas as ${now}HS conforme definido para iniciar as ${savedstarttime}HS..."
			loadmusics
			playmusic
			#$mpg123 --random -@ $playlist > $mpglogfile 2> $mpgerrorfile &
		;;
		stop)
			log "Parando musicas as ${now}HS conforme definido para para as ${savedendtime}HS..."
			log "Checando mpg123 daemon..."
			if [ "x$pidmpg" != "x" ]
			then
				log "rodando com PID $pidmpg"
				log "Finalizando mpg123..."
				kill $pidmpg > $mpglogfile 2> $mpgerrorfile
				log "Feito!"
			else
				log "parado"
			fi
			musicloop=0
		;;
		ignore)
			log "Ignorando inicio das musicas: musicloop=${musicloop} , start time=${savedstarttime} , end time=${savedendtime}, now=${now}"
		;;
	esac


}


check
