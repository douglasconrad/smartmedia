#!/bin/bash 

# variaveis de ambiente
conffile=smart.conf
basedir=`dirname $0`
debug=0
synctimeloop=60
pidfile=/tmp/smartmedia.pid
pidsufix=/tmp/smartmedia
mpgfifo=/tmp/mpgctl
mpglogfile=/tmp/mpg.log
mpgerrorfile=/tmp/mpg.error
trackfile=/tmp/trackposition
tracknumbersfile=/tmp/tracknumbers
mpg123=/usr/bin/mpg123
mplayer=/usr/bin/mplayer
rsync=/usr/bin/rsync
amixer=/usr/bin/amixer
errorlog=/tmp/smartmedia.err
player=mplayer
#musicdevice="3 1"
musicdevice="1 1"
track=0
tracknumbers=0
logfile=/tmp/smartmedia.log
PID=$$
echo $PID > $pidfile
app=`basename $0`

check(){
	for arg in $@
	do
		if [ "$arg" == "-d" ]
		then
			debug=1
		fi
	done

	logfile=/tmp/smartmedia.log

	if [ ! -x $rsync ]
	then
		debug=1
		log "Programa [rsync] nao encontrado!"
		log "Saindo da Aplicação"
		exit 1
	elif [ ! -x $mpg123 ]
	then
		debug=1
		log "Programa [mpg123] nao encontrado!"
		log "Saindo da Aplicação"
		exit 1
	fi
	load
}
printhelp() {
	echo -en "ERRO: Voce deve informar a origem e o destino da sincronizacao.\n"
	echo -en "Use:
	$0 [OPCOES] ORIGEM DESTINO
	Opcoes:
		-d : habilita um debug de tela

	Exemplo de uso:
		$0 usuario@192.168.10.1:/home/usuario/Musicas /home/Musicas\n\n"
}

log(){
	if [ $debug -eq 1 ]
	then
		echo -en "`date` - SMARTMEDIA - $1\n"
	fi
	echo -en "`date` - SMARTMEDIA - $1\n" >> $logfile
	PID=$$
	echo $PID > $pidfile.log
}

readconfig(){
	if [ ! -f $basedir/$conffile ]
	then
		log "ERRO: arquivo de configuração [$basedir/$conffile] nao existe!"
		log "Saindo da Aplicação"
		exit 1
	fi
	. $basedir/$conffile
	if [ "x$1" = "x1" ]
	then
		echo 0 > $trackfile
		echo 0 > $tracknumbersfile
	fi
	log "Configuracoes padroes carregadas"
}

testconnection(){
	server=$1
	log "Testando a conexao com o SERVER: $server"
	status=`ping -q -c 1 $server|grep "packet loss"|awk '{print $6}'|sed s/%//g`
	if [ "$status" -gt "0" ]
	then
		debug=1
		log "ERRO de conexao com o SERVER: $server"
		#log "Saindo da Aplicação"
		#exit 1
	else
		log "Conexao com o SERVER: $server OK"
	fi
}

gethost(){
	echo $sourceroot|awk -F: '{print $1}'|awk -F@ '{print $2}'
}

setmusic(){
	trackposition=`eval cat $trackfile`
	(( trackposition++ ))
	echo $trackposition > $trackfile
}
getmusic(){
	track=`eval cat $trackfile`
	tracknumbers=`eval cat $tracknumbersfile`
}

load(){
	log "Lendo configuracoes padroes..."
	readconfig 1
	log "Carregando as chaves de autenticação..."
	eval `ssh-agent -s`
	ssh-add $private_key
	log "Ajustando dispositivo de music para $musicdevice"
	$amixer cset numid=$musicdevice  2>/dev/null > /dev/null
}
sync(){
	PID=$$
	echo $PID > $pidfile.sync
	testconnection $defaultmediaserver
	log "Iniciando sincronia..."
	if [ "x$deletefromdest" == "xyes" ]
	then
		delete="--delete"
	fi
	log "Sincronizando musicas..."
	export RSYNC_PASSWORD=$defaultpassword
	$rsync -razql --no-motd -e "ssh -p $sshport -o 'StrictHostKeyChecking=no'" $defaultuser@$defaultmediaserver:$remotemusicpath $musicpath >> $logfile 2>> $logfile
	log "Sincronizando videos..."
	$rsync -razql --no-motd -e "ssh -p $sshport -o 'StrictHostKeyChecking=no'" $defaultuser@$defaultmediaserver:$remotevideopath $videopath >> $logfile 2>> $logfile

}

getskil(){
	testconnection $defaultmediaserver
	export RSYNC_PASSWORD=$defaultpassword
	
}

loadmusics(){
	log "Criando Playlist..."
	case $playlist_type in
		streaming)
			if [ "x$streaming_server" == "x" ]
			then
				log "ERROR no Streaming! Voce precisa definir o servidor de Streaming com a variavel [streaming_server]"
				log "Exemplo: streaming_server=http://192.168.56.1:8080"
				streaming_server=/dev/null
			fi
			playlist=$streaming_server
			log "Playlist criada com streaming de [$playlist]"
		;;
		*)
			playlist=$musicpath/music-playlist
			find $musicpath -type f -iname \*.mp3 > $playlist 2>> $errorlog
			tracknumbers=`cat $playlist|wc -l`
			echo $tracknumbers > $tracknumbersfile
			setmusic
			getmusic
			log "Playlist criada com $tracknumbers musicas"
		;;
	esac
}

readmpg(){
	#pidmpg=`ps -C mpg123 -o pid=`
	pidmpg=`ps -C mplayer -o pid=`
	echo >  $mpglogfile
	log "Checando player daemon..."
	if [ "x$pidmpg" != "x" ]
	then
		log "player ja rodando"
	else
		log "Iniciando daemon player..."
		rm -f $mpgfifo
		$mpg123 -R --fifo $mpgfifo > $mpglogfile 2> $mpgerrorfile &
		sleep 3
		chmod 666 $mpgfifo
	fi
	log "Iniciando scanner de musicas..."
	exec 3<$mpglogfile 
	while [ 1 ] 
	do 
		read -u3 line 
		if [[ $(echo "$line" |grep -c "@P 0") -eq 1 ]]
		then 
			playmusic 
		#elif [ "x$line" == "x" ]
		#then
		#	playmusic 
		fi 
	done	
		
}

playmusic(){
	getmusic
	log "Tocando a musica $track de um total de $tracknumbers"
	log "Checando se $track é maior ou igual a $tracknumbers"
	if [[ $track -ge $tracknumbers ]]
	then
		log "A Playlist chegou ao final das musicas. Total de $tracknumbers musicas foram tocadas"
		log "Reiniciando a Playlist..."
		echo 1 > $trackfile
	fi
	if [ "$musicaction" == "start" ]
	then
		setmusic
		log "Iniciando a musica $track da playlist $playlist...usando fifo $mpgfifo"
		echo "LOADLIST $track $playlist" > $mpgfifo
		log "Feito"
	fi
}

savepid(){
	echo $! > ${pidsufix}-$1.pid
}

getstatus(){
	# busca o status do programa passado como primeiro argumento.
	# retorna os seguintes status:
	# 0 - nao esta rodando
	# 1 - rodando com sucesso
	# 2 - rodando mas com pid diferente do registrado. Sincroniza o PID do processo rodando com o salvo no arquivo do PID.
	temppid=`head -1 ${pidsufix}-${1}.pid`
	case $1 in
		player)
			proc=$player
	;;
		smartmedia)
			proc=$1.sh
	;;
		*)
			proc=$1
	;;
	esac

	running=`ps -C $proc -o pid= | head -1 --`
	log "Buscando status do processo [$proc]! PID salvo:[$temppid], PID Real: [$running]"
	if [ "$running" == "$temppid" ]
	then
		log "Processo rodando com sucesso!"
		echo 1
	elif [ "x$running" != "x" ]
	then
		log "Processo rodando mas com PID divergente!"
		echo $running > ${pidsufix}-${1}.pid
		echo 2
	else
		log "Processo parado!"
		echo 0
	fi
}

getpid(){
	temppid=`head -1 ${pidsufix}-${1}.pid`
	case $1 in
		player)
			proc=$player
	;;
		smartmedia)
			proc=$1.sh
	;;
		*)
			proc=$1
	;;
	esac
	running=`ps -C $proc -o pid= | head -1 --`
	if [ "$running" == $temppid ]
	then
		echo $running
	elif [ "x$running" != "x" ]
	then
		echo $running > ${pidsufix}-${1}.pid
		echo $running
	fi
}

runmusic(){
	savedstarttime=`echo $musictimestart|sed s,:,,g`
	savedendtime=`echo $musictimeend|sed s,:,,g`
	now=`date '+%H%M'`
	musicaction="ignore"
	track=`eval cat $trackfile`
	tracknumbers=`eval cat $tracknumbersfile`
	if [ $musicloop -ge 1 ]
	then
		if [ "$now" -ge "$savedendtime" ]
		then
				musicaction="stop"
		elif [ $now -ge $savedstarttime ] && [ $now -le $savedendtime ]
		then
				musicaction="start"
		else
				musicaction="ignore"
		fi
	elif [ $now -ge $savedstarttime ] && [ $now -le $savedendtime ]
	then
		log "Ajustando para iniciar musicas...musicloop=${musicloop} , start time=${savedstarttime} , end time=${savedendtime}, now=${now}"
		musicaction="start"
		musicloop=$(expr $musicloop + 1)
	else
		if [ "x$pidmpg" != "x" ]
		then 
			musicaction="stop"
		else
			musicaction="ignore"
		fi
	fi

	case $musicaction in
		start)
			playerstatus=`getstatus player 2> /dev/null`
			log "Player com status: [$playerstatus]"
			if [ $playerstatus == 0 ]
			then
				log "Iniciando musicas as ${now}HS. Horario de inicio agendado: ${savedstarttime}HS."
				if [ -z $track ] || [ $track -le 0 ]
				then
					loadmusics
					log "Playlist $playlist carregada"
				fi
				#playmusic
				log "Ajustando volume default para $defaultvolume"
				$amixer -c 0 -- sset Master playback -20dB 2> /dev/null > /dev/null 
				$amixer -c 0 -- sset Master playback $defaultvolume% 2>/dev/null > /dev/null
				#$mpg123 --list $playlist -Z > $mpglogfile 2> $mpgerrorfile &
				log "Iniciando reproducao"
				nohup $mplayer $playlist  >> /tmp/mplayer.log 2> /dev/null &
				savepid player
			fi
		;;
		stop)
			log "Parando musicas as ${now}HS conforme definido para para as ${savedendtime}HS..."
			log "Checando player daemon..."
			pidmpg=`ps -C $player -o pid=`
			if [ "x$pidmpg" != "x" ]
			then
				log "rodando com PID $pidmpg"
				log "Finalizando player..."
				kill $pidmpg > $mpglogfile 2> $mpgerrorfile
				log "Feito!"
			else
				log "parado"
			fi
			musicloop=0
		;;
		ignore)
			log "Ignorando inicio das musicas: musicloop=${musicloop} , start time=${savedstarttime} , end time=${savedendtime}, now=${now}"
		;;
	esac


}

run(){
	PID=$$
	musicloop=0
	syncloop=0
	#readmpg &
	echo $PID > $pidfile.run
	while [ 1 ] 
	do 
		track=`eval cat $trackfile`
		tracknumbers=`eval cat $tracknumbersfile`
		readconfig
		runmusic
		#sync 
		sleep $synctimeloop
	done

}

case $1 in
	start)
		check $1 $2 $3
		run &
		savepid smartmedia
	;;
	stop)
		pidplayer=`getpid player`
		if [ "x$pidplayer" != "x" ]
		then
			log "Encerrando processo do player [$pidplayer]"
			echo "Encerrando processo do player [$pidplayer]"
			kill $pidplayer
		fi
		pid=`getpid smartmedia`
		if [ "x$pid" != "x" ]
		then
			log "Encerrando processo smartmedia [$pid]"
			echo "Encerrando processo smartmedia [$pid]"
			kill $pid
		else
			log "Smartmedia nao rodando"
			echo "Smartmedia nao rodando"
		fi
	;;
	*)
		echo "Use: $1 [start|stop]"
	;;
	esac

